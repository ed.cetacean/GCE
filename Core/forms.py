from django import forms
from .models import *

## PUESTO: ------------------------------------------------------------------ ##
class PuestoForm(forms.ModelForm):
    class Meta:
        model = Puesto
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"40", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"320", "placeholder":"Descripción"}),
            "departamentos": forms.CheckboxSelectMultiple(attrs={
                "class": "checkbox-select-multiple"}),
        }

class UpdatePuestoForm(forms.ModelForm):

    class Meta:
        model = Puesto
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"40", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"320", "placeholder":"Descripción"}),
            "departamentos": forms.CheckboxSelectMultiple(attrs={
                "class": "checkbox-select-multiple"}),
        }

## DEPARTAMENTO: ------------------------------------------------------------ ##
class DepartamentoForm(forms.ModelForm):

    class Meta:
        model = Departamento
        fields = "__all__"
        exclude = [ "fechacreacion" ]

        widgets = {
            "codigo" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"40", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"320", "placeholder":"Descripción"}),
            "numextension": forms.NumberInput(attrs={
                "type":"tel", "class":"form-control", "maxlength":"4", "placeholder":"Número de extensión"}),
            "puestos": forms.CheckboxSelectMultiple(attrs={
                "class": "checkbox-select-multiple"}),
        }

class UpdateDepartamentoForm(forms.ModelForm):

    class Meta:
        model = Departamento
        fields = "__all__"
        exclude = [ "fechacreacion" ]

        widgets = {
            "codigo" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"40", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"320", "placeholder":"Descripción"}),
            "numextension": forms.NumberInput(attrs={
                "type":"tel", "class":"form-control", "maxlength":"4", "placeholder":"Número de extensión"}),
            "puestos": forms.CheckboxSelectMultiple(attrs={
                "class": "checkbox-select-multiple"}),
        }

## EMPLEADO: ---------------------------------------------------------------- ##
class EmpleadoForm(forms.ModelForm):
    puesto = forms.ModelChoiceField(queryset=Puesto.objects.all(), label='Puesto',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), label='Departamento',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Empleado
        fields = "__all__"
        exclude = [ "fechacontrato" ]

        widgets = {
            "numero" : forms.NumberInput(attrs={"type":"text", "class":"form-control", "placeholder":"Número"}),
            "nombrepila" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"60", "placeholder":"Nombre pila"}),
            "appaterno" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"60", "placeholder":"Apellido paterno"}),
            "apmaterno" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"60", "placeholder":"Apellido materno"}),
            "numtel": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Número de teléfono","maxlength": "15"}),
            "email": forms.EmailInput(attrs={"type":"email", "class":"form-control", "maxlenght":"60", "placeholder":"Email"}),
        }

class UpdateEmpleadoForm(forms.ModelForm):
    puesto = forms.ModelChoiceField(queryset=Puesto.objects.all(), label='Puesto',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), label='Departamento',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Empleado
        fields = "__all__"
        exclude = [ "fechacontrato" ]

        widgets = {
            "numero" : forms.NumberInput(attrs={"type":"text", "class":"form-control", "placeholder":"Número"}),
            "nombrepila" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"60", "placeholder":"Nombre pila"}),
            "appaterno" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"60", "placeholder":"Apellido paterno"}),
            "apmaterno" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"60", "placeholder":"Apellido materno"}),
            "numtel": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Número de teléfono","maxlength": "15"}),
            "email": forms.EmailInput(attrs={"type":"email", "class":"form-control", "maxlenght":"60", "placeholder":"Email"}),
        }

## CURSO: ------------------------------------------------------------------- ##
class CursoForm(forms.ModelForm):

    class Meta:
        model = Curso
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Código"}),
            "nombre" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Descripción"}),
            "duracion" : forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Duración (en minutos)"}),
        }

class UpdateCursoForm(forms.ModelForm):

    class Meta:
        model = Curso
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Código"}),
            "nombre" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Descripción"}),
            "duracion" : forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Duración (en minutos)"}),
        }

## EXAMEN: ------------------------------------------------------------------ ##
class ExamenForm(forms.ModelForm):
    curso = forms.ModelChoiceField(queryset=Curso.objects.all(), label='Curso',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Examen
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"60", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"320", "placeholder":"Descripción"}),
            "tiempolimite": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Duración"}),
            "intentosmax": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Duración"}),
        }

class UpdateExamenForm(forms.ModelForm):
    curso = forms.ModelChoiceField(queryset=Curso.objects.all(), label='Curso',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Examen
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"60", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"320", "placeholder":"Descripción"}),
            "tiempolimite": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Duración"}),
            "intentosmax": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Duración"}),
        }

## TEMAS: ------------------------------------------------------------------- ##
class TemasForm(forms.ModelForm):
    curso = forms.ModelChoiceField(queryset=Curso.objects.all(), label='Curso',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Temas
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"80", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"320", "placeholder":"Descripción"}),
        }

class UpdateTemasForm(forms.ModelForm):
    curso = forms.ModelChoiceField(queryset=Curso.objects.all(), label='Curso',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Temas
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"80", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "maxlenght":"320", "placeholder":"Descripción"}),
        }

## MATERIALES: -------------------------------------------------------------- ##
class MaterialesForm(forms.ModelForm):
    curso = forms.ModelChoiceField(queryset=Curso.objects.all(), label='Curso',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Materiales
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"80", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"320", "placeholder":"Descripción"}),
        }

class UpdateMaterialesForm(forms.ModelForm):
    curso = forms.ModelChoiceField(queryset=Curso.objects.all(), label='Curso',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Materiales
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"4", "placeholder":"Codigo"}),
            "nombre" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"80", "placeholder":"Nombre"}),
            "descripcion" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"320", "placeholder":"Descripción"}),
        }

## RESPUESTAS: -------------------------------------------------------------- ##
class RespuestasForm(forms.ModelForm):

    class Meta:
        model = Respuestas
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"4", "placeholder":"Codigo"}),
            "descripcion" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"320", "placeholder":"Descripción"}),
        }

class UpdateRespuestasForm(forms.ModelForm):

    class Meta:
        model = Respuestas
        fields = "__all__"

        widgets = {
            "codigo" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"4", "placeholder":"Codigo"}),
            "descripcion" : forms.TextInput(attrs={
                "type":"text", "class":"form-control", "maxlength":"320", "placeholder":"Descripción"}),
        }

## PREGUNTAS: --------------------------------------------------------------- ##
class PreguntasForm(forms.ModelForm):
    tema = forms.ModelChoiceField(queryset=Temas.objects.all(), label='Tema',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Preguntas
        fields = "__all__"

        widgets = {
            "codigo": forms.TextInput(attrs={"type": "text", "class": "form-control", "maxlenght": "4", "placeholder": "Codigo"}),
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "maxlenght": "320", "placeholder": "Descripción"}),
        }

class UpdatePreguntasForm(forms.ModelForm):
    tema = forms.ModelChoiceField(queryset=Temas.objects.all(), label='Tema',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Preguntas
        fields = "__all__"

        widgets = {
            "codigo": forms.TextInput(attrs={"type": "text", "class": "form-control", "maxlenght": "4", "placeholder": "Codigo"}),
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "maxlenght": "320", "placeholder": "Descripción"}),
        }

## PREGUNTAS-RESPUESTAS: ---------------------------------------------------- ##
class PregRespForm(forms.ModelForm):
    examen = forms.ModelChoiceField(queryset=Examen.objects.all(), label='Examen',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    pregunta = forms.ModelChoiceField(queryset=Preguntas.objects.all(), label='Pregunta',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    respuesta = forms.ModelChoiceField(queryset=Respuestas.objects.all(), label='Respuesta',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    respcorrecta = forms.BooleanField(label="¿Es correcta?", initial=False, required=False)

    class Meta:
        model = PregResp
        fields = "__all__"
        exclude = [ "num" ]

class UpdatePregRespForm(forms.ModelForm):
    examen = forms.ModelChoiceField(queryset=Examen.objects.all(), label='Examen',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    respuesta = forms.ModelChoiceField(queryset=Respuestas.objects.all(), label='Respuesta',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    respcorrecta = forms.BooleanField(label="¿Es correcta?", widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}), required=False)

    class Meta:
        model = PregResp
        fields = "__all__"
        exclude = [ "num", "pregunta" ]

## CAPACITACION: ------------------------------------------------------------ ##
class CapacitacionForm(forms.ModelForm):
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), label='Departamento',
        widget=forms.Select(attrs={'class': 'form-control',}), required=False
    )
    curso = forms.ModelChoiceField(queryset=Curso.objects.all(), label='Curso',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Capacitacion
        fields = "__all__"

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero"}),
            "fechainicio" : forms.DateInput(attrs={"type":"date", "class":"form-control", "placeholder":"Fecha de inicio"}),
            "fechafinal" : forms.DateInput(attrs={"type":"date", "class":"form-control", "placeholder":"Fecha final"}),
            "capacidad": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Capacidad"}),
            "instructores": forms.CheckboxSelectMultiple(attrs={"class": "checkbox-select-multiple"}),
        }

class UpdateCapacitacionForm(forms.ModelForm):
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all(), label='Departamento',
        widget=forms.Select(attrs={'class': 'form-control',}), required=False
    )
    curso = forms.ModelChoiceField(queryset=Curso.objects.all(), label='Curso',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Capacitacion
        fields = "__all__"

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero"}),
            "fechainicio" : forms.DateInput(attrs={"type":"date", "class":"form-control", "placeholder":"Fecha de inicio"}),
            "fechafinal" : forms.DateInput(attrs={"type":"date", "class":"form-control", "placeholder":"Fecha final"}),
            "capacidad": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Capacidad"}),
            "instructores": forms.CheckboxSelectMultiple(attrs={"class": "checkbox-select-multiple"}),
        }

## INSTRUCTOR: -------------------------------------------------------------- ##
class InstructorForm(forms.ModelForm):

    class Meta:
        model = Instructor
        fields = "__all__"
        exclude = [ "fechacontrato" ]

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Codigo"}),
            "nombrepila" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"NombrePila"}),
            "appaterno" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"ApPaterno"}),
            "apmaterno" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"ApMaterno"}),
            "numtel": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Número de teléfono","maxlength": "15"}),
            "email": forms.EmailInput(attrs={"type":"email", "class":"form-control", "placeholder":"Email"}),
            "contratacion": forms.Select(attrs={"type":"select","class":"form-select"}),
        }

class UpdateInstructorForm(forms.ModelForm):

    class Meta:
        model = Instructor
        fields = "__all__"
        exclude = [ "fechacontrato" ]

        model = Instructor
        fields = "__all__"
        exclude = [ "fechacontrato" ]

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Codigo"}),
            "nombrepila" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"NombrePila"}),
            "appaterno" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"ApPaterno"}),
            "apmaterno" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"ApMaterno"}),
            "numtel": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Número de teléfono","maxlength": "15"}),
            "email": forms.EmailInput(attrs={"type":"email", "class":"form-control", "placeholder":"Email"}),
            "contratacion": forms.Select(attrs={"type":"select","class":"form-select"}),
        }

## INSCRIPCION:-------------------------------------------------------------- ##
class InscripcionForm(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Empleado.objects.all(), label='Empleado',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    capacitacion = forms.ModelChoiceField(queryset=Capacitacion.objects.all(), label='Capacitación',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Inscripcion
        fields = "__all__"
        exclude = [ "fechainscripcion" ]

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero"}),
        }

class UpdateInscripcionForm(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Empleado.objects.all(), label='Empleado',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    capacitacion = forms.ModelChoiceField(queryset=Capacitacion.objects.all(), label='Capacitación',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Inscripcion
        fields = "__all__"
        exclude = [ "fechainscripcion" ]

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero"}),
        }

## RESPUESTAS-EMPLEADO: ----------------------------------------------------- ##
class RespEmpleadoForm(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Empleado.objects.all(), label='Empleado',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    pregunta = forms.ModelChoiceField(queryset=Preguntas.objects.all(), label='Pregunta',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    respuesta = forms.ModelChoiceField(queryset=Respuestas.objects.all(), label='Respuesta',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    capacitacion = forms.ModelChoiceField(queryset=Capacitacion.objects.all(), label='Capacitación',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = RespuestasEmpleado
        fields = "__all__"
        exclude = [ "intentosrealizados", "num" ]

class UpdateRespEmpleadoForm(forms.ModelForm):
    respuesta = forms.ModelChoiceField(queryset=Respuestas.objects.all(), label='Respuesta',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = RespuestasEmpleado
        fields = "__all__"
        exclude = [ "intentosrealizados", "empleado", "num", "capacitacion", "pregunta" ]

    def get_form_kwargs(self):
        kwargs = super(UpdateRespEmpleadoForm, self).get_form_kwargs()
        kwargs['instance'] = self.get_object()
        return kwargs

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        CapacitacionActual = self.initial.get('capacitacion') or self.instance.capacitacion

        if isinstance(CapacitacionActual, int):
            CapacitacionActual = Capacitacion.objects.get(pk=CapacitacionActual)

        if CapacitacionActual:
            CapacitacionCurso = CapacitacionActual.curso_id
            CapacitacionExamen = Examen.objects.filter(curso_id=CapacitacionCurso).first()

            if CapacitacionExamen:

                PreguntaResp = self.initial.get('pregunta') or self.instance.pregunta
                RespuestasPreg = Respuestas.objects.filter(pregresp__pregunta=PreguntaResp, pregresp__examen=CapacitacionExamen)
                self.fields['respuesta'].queryset = RespuestasPreg

## EMPLEADO_EXAMEN: --------------------------------------------------------- ##
class InscripcionForm(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Empleado.objects.all(), label='Empleado',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    capacitacion = forms.ModelChoiceField(queryset=Capacitacion.objects.all(), label='Capacitación',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Inscripcion
        fields = "__all__"
        exclude = [ "fechainscripcion" ]

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero"}),
        }

class UpdateInscripcionForm(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Empleado.objects.all(), label='Empleado',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    capacitacion = forms.ModelChoiceField(queryset=Capacitacion.objects.all(), label='Capacitación',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Inscripcion
        fields = "__all__"
        exclude = [ "fechainscripcion" ]

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero"}),
        }

## CONSTANCIA: -------------------------------------------------------------- ##
class ConstanciaForm(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Empleado.objects.all(), label='Empleado',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    capacitacion = forms.ModelChoiceField(queryset=Capacitacion.objects.all(), label='Capacitación',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    examen = forms.ModelChoiceField(queryset=Examen.objects.all(), label='Capacitación',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Constancia
        fields = "__all__"
        exclude = [ "fechaemision" ]

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero"}),
            "calificacion": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Calificacion"}),
        }

class UpdateConstanciaForm(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Empleado.objects.all(), label='Empleado',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    capacitacion = forms.ModelChoiceField(queryset=Capacitacion.objects.all(), label='Capacitación',
        widget=forms.Select(attrs={'class': 'form-control',})
    )
    examen = forms.ModelChoiceField(queryset=Examen.objects.all(), label='Capacitación',
        widget=forms.Select(attrs={'class': 'form-control',})
    )

    class Meta:
        model = Constancia
        fields = "__all__"
        exclude = [ "fechaemision" ]

        widgets = {
            "numero" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero"}),
            "calificacion": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Calificacion"}),
        }

## -------------------------------------------------------------------------- ##