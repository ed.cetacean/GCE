
from django.db import models

class Puesto(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    nombre = models.CharField(db_column='Nombre', max_length=40, null=False)
    descripcion = models.CharField(db_column='Descripcion', max_length=320, null=False)
    departamentos = models.ManyToManyField('Departamento', through='PuestoDepto')

    def __str__(self):
        return self.nombre

    class Meta:
        managed = False
        db_table = 'PUESTO'

class Departamento(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    nombre = models.CharField(db_column='Nombre', max_length=40, null=False)
    descripcion = models.CharField(db_column='Descripcion', max_length=320, null=False)
    numextension = models.CharField(db_column='NumExtension', unique=True, max_length=4, null=False)
    fechacreacion = models.DateField(db_column='FechaCreacion', blank=True, null=True, auto_now_add=True)
    puestos = models.ManyToManyField('Puesto', through='PuestoDepto')

    def __str__(self):
        return self.nombre

    class Meta:
        managed = False
        db_table = 'DEPARTAMENTO'

class PuestoDepto(models.Model):
    puesto = models.ForeignKey(Puesto, on_delete=models.CASCADE, db_column='Puesto')
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE, db_column='Departamento')

    class Meta:
        managed = False
        db_table = 'PUESTO_DEPTO'
        unique_together = ('puesto', 'departamento')

class Empleado(models.Model):
    numero = models.AutoField(db_column='Numero', primary_key=True)
    nombrepila = models.CharField(db_column='NombrePila', max_length=60, null=False)
    appaterno = models.CharField(db_column='ApPaterno', max_length=60, blank=True, null=True)
    apmaterno = models.CharField(db_column='ApMaterno', max_length=60, blank=True, null=True)
    numtel = models.CharField(db_column='NumTel', max_length=15, blank=True, null=True)
    puesto = models.ForeignKey('Puesto', models.CASCADE, db_column='Puesto', blank=True, null=True)
    departamento = models.ForeignKey(Departamento, models.CASCADE, db_column='Departamento', blank=True, null=True)
    email = models.CharField(db_column='Email', unique=True, max_length=60, null=False)
    fechacontrato = models.DateField(db_column='FechaContrato', blank=True, null=True, auto_now_add=True)

    def __str__(self):
        nombre_completo = f"{self.nombrepila} {self.appaterno or ''} {self.apmaterno or ''}".strip()
        return nombre_completo

    class Meta:
        managed = False
        db_table = 'EMPLEADO'

class Curso(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    nombre = models.CharField(db_column='Nombre', max_length=40, null=False)
    descripcion = models.CharField(db_column='Descripcion', max_length=320, null=False)
    duracion = models.IntegerField(db_column='Duracion', null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        managed = False
        db_table = 'CURSO'

class Temas(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    nombre = models.CharField(db_column='Nombre', max_length=80, null=False)
    descripcion = models.CharField(db_column='Descripcion', max_length=320, null=False)
    curso = models.ForeignKey(Curso, models.CASCADE, db_column='Curso', null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        managed = False
        db_table = 'TEMAS'

class Examen(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    nombre = models.CharField(db_column='Nombre', max_length=60, null=False)
    descripcion = models.CharField(db_column='Descripcion', max_length=320, null=False)
    tiempolimite = models.IntegerField(db_column='TiempoLimite', null=False)
    intentosmax = models.IntegerField(db_column='IntentosMax', null=False)
    curso = models.ForeignKey(Curso, models.CASCADE, db_column='Curso', blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        managed = False
        db_table = 'EXAMEN'

class Materiales(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    nombre = models.CharField(db_column='Nombre', max_length=80, null=False)
    descripcion = models.CharField(db_column='Descripcion', max_length=320, null=False)
    curso = models.ForeignKey(Curso, models.CASCADE, db_column='Curso', null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        managed = False
        db_table = 'MATERIALES'

class Respuestas(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    descripcion = models.CharField(db_column='Descripcion', max_length=320, null=False)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'RESPUESTAS'

class Preguntas(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    descripcion = models.CharField(db_column='Descripcion', max_length=320, null=False)
    tema = models.ForeignKey('Temas', models.CASCADE, db_column='Tema', blank=True, null=True)

    def __str__(self):
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'PREGUNTAS'

class PregResp(models.Model):
    num = models.AutoField(db_column='ID', primary_key=True)
    pregunta = models.ForeignKey(Preguntas, models.CASCADE, db_column='Pregunta')
    respuesta = models.ForeignKey(Respuestas, models.CASCADE, db_column='Respuesta')
    respcorrecta = models.BooleanField(db_column='RespCorrecta')
    examen = models.ForeignKey(Examen, models.CASCADE, db_column='Examen', null=False)

    def respcorrecta_as_boolean(self):
        return bool(int.from_bytes(self.respcorrecta, byteorder='big'))

    def __str__(self):
        return str(self.num)

    class Meta:
        managed = False
        db_table = 'PREG_RESP'

class Capacitacion(models.Model):
    numero = models.AutoField(db_column='Numero', primary_key=True)
    fechainicio = models.DateField(db_column='FechaInicio', null=False)
    fechafinal = models.DateField(db_column='FechaFinal', null=False)
    capacidad = models.IntegerField(db_column='Capacidad', null=False)
    departamento = models.ForeignKey('Departamento', models.CASCADE, db_column='Departamento', blank=True, null=True)
    curso = models.ForeignKey(Curso, models.CASCADE, db_column='Curso', null=False)
    instructores = models.ManyToManyField('Instructor', through='CapctInstr')

    def __str__(self):
        return str(self.numero)

    class Meta:
        managed = False
        db_table = 'CAPACITACION'

class Instructor(models.Model):
    numero = models.AutoField(db_column='Numero', primary_key=True)
    nombrepila = models.CharField(db_column='NombrePila', max_length=60, null=False)
    appaterno = models.CharField(db_column='ApPaterno', max_length=60, blank=True, null=True)
    apmaterno = models.CharField(db_column='ApMaterno', max_length=60, blank=True, null=True)
    numtel = models.CharField(db_column='NumTel', max_length=15, null=False)
    email = models.CharField(db_column='Email', unique=True, max_length=60, null=False)
    fechacontrato = models.DateField(db_column='FechaContrato', blank=True, null=True, auto_now_add=True)
    contratacion = models.CharField(max_length=10, choices=[('Interna', 'Interna'), ('Externa', 'Externa')])

    def __str__(self):
        nombre_completo = f"{self.nombrepila} {self.appaterno or ''} {self.apmaterno or ''}".strip()
        return nombre_completo

    class Meta:
        managed = False
        db_table = 'INSTRUCTOR'

class CapctInstr(models.Model):
    capacitacion = models.ForeignKey(Capacitacion, models.CASCADE, db_column='Capacitacion')
    instructor = models.ForeignKey(Instructor, models.CASCADE, db_column='Instructor')

    class Meta:
        managed = False
        db_table = 'CAPCT_INSTR'
        unique_together = ('capacitacion', 'instructor')

class Inscripcion(models.Model):
    numero = models.AutoField(db_column='Numero', primary_key=True)
    fechainscripcion = models.DateField(db_column='FechaInscripcion', blank=True, null=True, auto_now_add=True)
    empleado = models.ForeignKey(Empleado, models.CASCADE, db_column='Empleado', null=False)
    capacitacion = models.ForeignKey(Capacitacion, models.CASCADE, db_column='Capacitacion', null=False)

    def __str__(self):
        return str(self.numero)

    class Meta:
        managed = False
        db_table = 'INSCRIPCION'

class RespuestasEmpleado(models.Model):
    num = models.AutoField(db_column='ID', primary_key=True)
    empleado = models.ForeignKey(Empleado, models.CASCADE, db_column='Empleado')
    pregunta = models.ForeignKey(Preguntas, models.CASCADE, db_column='Pregunta')
    respuesta = models.ForeignKey(Respuestas, models.CASCADE, db_column='Respuesta')
    intentosrealizados = models.IntegerField(db_column='IntentosRealizados', default=1)
    capacitacion = models.ForeignKey(Capacitacion, models.CASCADE, db_column='Capacitacion')

    def __str__(self):
        return str(self.num)

    class Meta:
        managed = False
        db_table = 'RESPUESTAS_EMPLEADO'

class EmpleadoExamen(models.Model):
    num = models.AutoField(db_column='ID', primary_key=True)
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, db_column='Empleado')
    examen = models.ForeignKey(Examen, on_delete=models.CASCADE, db_column='Examen')
    capacitacion = models.ForeignKey(Capacitacion, on_delete=models.CASCADE, db_column='Capacitacion')
    calificacion = models.FloatField(db_column='Calificacion', blank=True, null=True)
    fechaelaboracion = models.DateField(db_column='FechaElaboracion', blank=True, null=True, auto_now_add=True)

    def __str__(self):
        return str(self.num)

    class Meta:
        managed = False
        db_table = 'EMPLEADO_EXAMEN'

class Constancia(models.Model):
    numero = models.AutoField(db_column='Numero', primary_key=True)
    fechaemision = models.DateField(db_column='FechaEmision', blank=True, null=True, auto_now_add=True)
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, db_column='Empleado')
    capacitacion = models.ForeignKey(Capacitacion, on_delete=models.CASCADE, db_column='Capacitacion')
    examen = models.ForeignKey(Examen, on_delete=models.CASCADE, db_column='Examen')
    calificacion = models.FloatField(db_column='Calificacion', blank=True, null=True)

    def __str__(self):
        return str(self.numero)

    class Meta:
        managed = False
        db_table = 'CONSTANCIA'

# ---------------------------------------------------------------------------- #