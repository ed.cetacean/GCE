from . import views
from django.urls import path

app_name = "Core"
urlpatterns = [

    ## PUESTOS:
    path('list/puesto/', views.ListPuesto.as_view(), name="ListPuesto"),
    path('create/puesto/', views.CreatePuesto.as_view(), name="CreatePuesto"),
    path('detail/puesto/<str:pk>/', views.DetailPuesto.as_view(), name="DetailPuesto"),
    path('update/puesto/<str:pk>/', views.UpdatePuesto.as_view(), name="UpdatePuesto"),
    path('delete/puesto/<str:pk>/', views.DeletePuesto.as_view(), name="DeletePuesto"),

    ## DEPARTAMENTOS:
    path('list/depto/', views.ListDepartamento.as_view(), name="ListDepartamento"),
    path('create/depto/', views.CreateDepartamento.as_view(), name="CreateDepartamento"),
    path('detail/depto/<str:pk>/', views.DetailDepartamento.as_view(), name="DetailDepartamento"),
    path('update/depto/<str:pk>/', views.UpdateDepartamento.as_view(), name="UpdateDepartamento"),
    path('delete/depto/<str:pk>/', views.DeleteDepartamento.as_view(), name="DeleteDepartamento"),

    ## EMPLEADOS:
    path('list/empleado/', views.ListEmpleado.as_view(), name="ListEmpleado"),
    path('create/empleado/', views.CreateEmpleado.as_view(), name="CreateEmpleado"),
    path('detail/empleado/<int:pk>/', views.DetailEmpleado.as_view(), name="DetailEmpleado"),
    path('update/empleado/<int:pk>/', views.UpdateEmpleado.as_view(), name="UpdateEmpleado"),
    path('delete/empleado/<int:pk>/', views.DeleteEmpleado.as_view(), name="DeleteEmpleado"),

    ## CURSOS:
    path('list/cursos/', views.ListCurso.as_view(), name="ListCurso"),
    path('create/curso/', views.CreateCurso.as_view(), name="CreateCurso"),
    path('detail/curso/<str:pk>/', views.DetailCurso.as_view(), name="DetailCurso"),
    path('update/curso/<str:pk>/', views.UpdateCurso.as_view(), name="UpdateCurso"),
    path('delete/curso/<str:pk>/', views.DeleteCurso.as_view(), name="DeleteCurso"),

    ## EXAMENES:
    path('list/examen/', views.ListExamen.as_view(), name="ListExamen"),
    path('create/examen/', views.CreateExamen.as_view(), name="CreateExamen"),
    path('detail/examen/<str:pk>/', views.DetailExamen.as_view(), name="DetailExamen"),
    path('update/examen/<str:pk>/', views.UpdateExamen.as_view(), name="UpdateExamen"),
    path('delete/examen/<str:pk>/', views.DeleteExamen.as_view(), name="DeleteExamen"),

    ## TEMAS:
    path('list/temas/', views.ListTemas.as_view(), name="ListTemas"),
    path('create/temas/', views.CreateTemas.as_view(), name="CreateTemas"),
    path('detail/temas/<str:pk>/', views.DetailTemas.as_view(), name="DetailTemas"),
    path('update/temas/<str:pk>/', views.UpdateTemas.as_view(), name="UpdateTemas"),
    path('delete/temas/<str:pk>/', views.DeleteTemas.as_view(), name="DeleteTemas"),

    ## MATERIALES:
    path('list/materiales/', views.ListMateriales.as_view(), name="ListMateriales"),
    path('create/materiales/', views.CreateMateriales.as_view(), name="CreateMateriales"),
    path('detail/materiales/<str:pk>/', views.DetailMateriales.as_view(), name="DetailMateriales"),
    path('update/materiales/<str:pk>/', views.UpdateMateriales.as_view(), name="UpdateMateriales"),
    path('delete/materiales/<str:pk>/', views.DeleteMateriales.as_view(), name="DeleteMateriales"),

    ## PREGUNTAS:
    path('list/preguntas/', views.ListPreguntas.as_view(), name="ListPreguntas"),
    path('create/preguntas/', views.CreatePreguntas.as_view(), name="CreatePreguntas"),
    path('detail/preguntas/<str:pk>/', views.DetailPreguntas.as_view(), name="DetailPreguntas"),
    path('update/preguntas/<str:pk>/', views.UpdatePreguntas.as_view(), name="UpdatePreguntas"),
    path('delete/preguntas/<str:pk>/', views.DeletePreguntas.as_view(), name="DeletePreguntas"),

    ## RESPUESTAS:
    path('list/respuestas/', views.ListRespuestas.as_view(), name="ListRespuestas"),
    path('create/respuestas/', views.CreateRespuestas.as_view(), name="CreateRespuestas"),
    path('detail/respuestas/<str:pk>/', views.DetailRespuestas.as_view(), name="DetailRespuestas"),
    path('update/respuestas/<str:pk>/', views.UpdateRespuestas.as_view(), name="UpdateRespuestas"),
    path('delete/respuestas/<str:pk>/', views.DeleteRespuestas.as_view(), name="DeleteRespuestas"),

    ## PREGUNTAS-RESPUESTAS:
    path('list/resp_examen/', views.ListPregResp.as_view(), name="ListPregResp"),
    path('create/resp_examen/', views.CreatePregResp.as_view(), name="CreatePregResp"),
    path('detail/resp_examen/<int:pk>/', views.DetailPregResp.as_view(), name="DetailPregResp"),
    path('update/resp_examen/<int:pk>/', views.UpdatePregResp.as_view(), name="UpdatePregResp"),
    path('delete/resp_examen/<int:pk>/', views.DeletePregResp.as_view(), name="DeletePregResp"),

    ## CAPACITACIONES:
    path('list/capacitacion/', views.ListCapacitacion.as_view(), name="ListCapacitacion"),
    path('create/capacitacion/', views.CreateCapacitacion.as_view(), name="CreateCapacitacion"),
    path('detail/capacitacion/<int:pk>/', views.DetailCapacitacion.as_view(), name="DetailCapacitacion"),
    path('update/capacitacion/<int:pk>/', views.UpdateCapacitacion.as_view(), name="UpdateCapacitacion"),
    path('delete/capacitacion/<int:pk>/', views.DeleteCapacitacion.as_view(), name="DeleteCapacitacion"),

    ## INSTRUCTOR:
    path('list/instructor/', views.ListInstructor.as_view(), name="ListInstructor"),
    path('create/instructor/', views.CreateInstructor.as_view(), name="CreateInstructor"),
    path('detail/instructor/<int:pk>/', views.DetailInstructor.as_view(), name="DetailInstructor"),
    path('update/instructor/<int:pk>/', views.UpdateInstructor.as_view(), name="UpdateInstructor"),
    path('delete/instructor/<int:pk>/', views.DeleteInstructor.as_view(), name="DeleteInstructor"),

    ## INSCRIPCIONES:
    path('list/inscripcion/', views.ListInscripcion.as_view(), name="ListInscripcion"),
    path('create/inscripcion/', views.CreateInscripcion.as_view(), name="CreateInscripcion"),
    path('detail/inscripcion/<int:pk>/', views.DetailInscripcion.as_view(), name="DetailInscripcion"),
    path('update/inscripcion/<int:pk>/', views.UpdateInscripcion.as_view(), name="UpdateInscripcion"),
    path('delete/inscripcion/<int:pk>/', views.DeleteInscripcion.as_view(), name="DeleteInscripcion"),

    ## CONSTANCIAS:
    path('list/constancia/', views.ListConstancia.as_view(), name="ListConstancia"),
    path('create/constancia/', views.CreateConstancia.as_view(), name="CreateConstancia"),
    path('detail/constancia/<int:pk>/', views.DetailConstancia.as_view(), name="DetailConstancia"),
    path('update/constancia/<int:pk>/', views.UpdateConstancia.as_view(), name="UpdateConstancia"),
    path('delete/constancia/<int:pk>/', views.DeleteConstancia.as_view(), name="DeleteConstancia"),

    ## RESPUESTAS-EMPLEADO:
    path('list/resp/', views.ListRespEmpleado.as_view(), name="ListRespEmpleado"),
    path('create/resp/', views.CreateRespEmpleado.as_view(), name="CreateRespEmpleado"),
    path('detail/resp/<int:pk>/', views.DetailRespEmpleado.as_view(), name="DetailRespEmpleado"),
    path('update/resp/<int:pk>/', views.UpdateRespEmpleado.as_view(), name="UpdateRespEmpleado"),
    path('delete/resp/<int:pk>/', views.DeleteRespEmpleado.as_view(), name="DeleteRespEmpleado"),
]