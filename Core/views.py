from django.views import generic
from django.shortcuts import render
from django.urls import reverse_lazy

from .forms import *
from .models import *

## DEPARTAMENTO: ------------------------------------------------------------ ##

## CREATE:
class CreateDepartamento(generic.CreateView):
    model = Departamento
    form_class = DepartamentoForm
    template_name = "Core/Departamento/Create.html"
    success_url = reverse_lazy('Core:ListDepartamento')

## LIST:
class ListDepartamento(generic.View):
    template_name = "Core/Departamento/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Departamento.objects.all()

        self.context = {
            "DPconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateDepartamento(generic.UpdateView):
    model = Departamento
    form_class = UpdateDepartamentoForm
    template_name = "Core/Departamento/Update.html"
    success_url = reverse_lazy('Core:ListDepartamento')

## DELETE:
class DeleteDepartamento(generic.DeleteView):
    model = Departamento
    template_name = "Core/Departamento/Delete.html"
    success_url = reverse_lazy('Core:ListDepartamento')

## DETAIL:
class DetailDepartamento(generic.DetailView):
    template_name = "Core/Departamento/Detail.html"
    model = Departamento

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['puestos'] = self.object.puestos.all()
        return context

## PUESTO: ------------------------------------------------------------------ ##

## CREATE:
class CreatePuesto(generic.CreateView):
    model = Puesto
    form_class = PuestoForm
    template_name = "Core/Puesto/Create.html"
    success_url = reverse_lazy('Core:ListPuesto')

## LIST:
class ListPuesto(generic.View):
    template_name = "Core/Puesto/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Puesto.objects.all()

        self.context = {
            "PTconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdatePuesto(generic.UpdateView):
    model = Puesto
    form_class = UpdatePuestoForm
    template_name = "Core/Puesto/Update.html"
    success_url = reverse_lazy('Core:ListPuesto')

## DELETE:
class DeletePuesto(generic.DeleteView):
    model = Puesto
    template_name = "Core/Puesto/Delete.html"
    success_url = reverse_lazy('Core:ListPuesto')

## DETAIL:
class DetailPuesto(generic.DetailView):
    template_name = "Core/Puesto/Detail.html"
    model = Puesto

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['departamentos'] = self.object.departamentos.all()
        return context

## EMPLEADO: ---------------------------------------------------------------- ##

## CREATE:
class CreateEmpleado(generic.CreateView):
    model = Empleado
    form_class = EmpleadoForm
    template_name = "Core/Empleado/Create.html"
    success_url = reverse_lazy('Core:ListEmpleado')

## LIST:
class ListEmpleado(generic.View):
    template_name = "Core/Empleado/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Empleado.objects.all()

        self.context = {
            "EMconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateEmpleado(generic.UpdateView):
    model = Empleado
    form_class = UpdateEmpleadoForm
    template_name = "Core/Empleado/Update.html"
    success_url = reverse_lazy('Core:ListEmpleado')

## DELETE:
class DeleteEmpleado(generic.DeleteView):
    model = Empleado
    template_name = "Core/Empleado/Delete.html"
    success_url = reverse_lazy('Core:ListEmpleado')

## DETAIL:
class DetailEmpleado(generic.DetailView):
    template_name = "Core/Empleado/Detail.html"
    model = Empleado

## CURSO: ------------------------------------------------------------------- ##

## CREATE:
class CreateCurso(generic.CreateView):
    model = Curso
    form_class = CursoForm
    template_name = "Core/Curso/Create.html"
    success_url = reverse_lazy('Core:ListCurso')

## LIST:
class ListCurso(generic.View):
    template_name = "Core/Curso/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Curso.objects.all()

        self.context = {
            "CRconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateCurso(generic.UpdateView):
    model = Curso
    form_class = UpdateCursoForm
    template_name = "Core/Curso/Update.html"
    success_url = reverse_lazy('Core:ListCurso')

## DELETE:
class DeleteCurso(generic.DeleteView):
    model = Curso
    template_name = "Core/Curso/Delete.html"
    success_url = reverse_lazy('Core:ListCurso')

## DETAIL:
class DetailCurso(generic.DetailView):
    template_name = "Core/Curso/Detail.html"
    model = Curso

## EXAMEN: ------------------------------------------------------------------ ##

## CREATE:
class CreateExamen(generic.CreateView):
    model = Examen
    form_class = ExamenForm
    template_name = "Core/Examen/Create.html"
    success_url = reverse_lazy('Core:ListExamen')

## LIST:
class ListExamen(generic.View):
    template_name = "Core/Examen/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Examen.objects.all()

        self.context = {
            "EXconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateExamen(generic.UpdateView):
    model = Examen
    form_class = UpdateExamenForm
    template_name = "Core/Examen/Update.html"
    success_url = reverse_lazy('Core:ListExamen')

## DELETE:
class DeleteExamen(generic.DeleteView):
    model = Examen
    template_name = "Core/Examen/Delete.html"
    success_url = reverse_lazy('Core:ListExamen')

## DETAIL:
class DetailExamen(generic.DetailView):
    template_name = "Core/Examen/Detail.html"
    model = Examen

## TEMAS: ------------------------------------------------------------------- ##

## CREATE:
class CreateTemas(generic.CreateView):
    model = Temas
    form_class = TemasForm
    template_name = "Core/Temas/Create.html"
    success_url = reverse_lazy('Core:ListTemas')

## LIST:
class ListTemas(generic.View):
    template_name = "Core/Temas/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Temas.objects.all()

        self.context = {
            "TMconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateTemas(generic.UpdateView):
    model = Temas
    form_class = UpdateTemasForm
    template_name = "Core/Temas/Update.html"
    success_url = reverse_lazy('Core:ListTemas')

## DELETE:
class DeleteTemas(generic.DeleteView):
    model = Temas
    template_name = "Core/Temas/Delete.html"
    success_url = reverse_lazy('Core:ListTemas')

## DETAIL:
class DetailTemas(generic.DetailView):
    template_name = "Core/Temas/Detail.html"
    model = Temas

## MATERIALES: -------------------------------------------------------------- ##

## CREATE:
class CreateMateriales(generic.CreateView):
    model = Materiales
    form_class = MaterialesForm
    template_name = "Core/Materiales/Create.html"
    success_url = reverse_lazy('Core:ListMateriales')

## LIST:
class ListMateriales(generic.View):
    template_name = "Core/Materiales/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Materiales.objects.all()

        self.context = {
            "MTconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateMateriales(generic.UpdateView):
    model = Materiales
    form_class = UpdateMaterialesForm
    template_name = "Core/Materiales/Update.html"
    success_url = reverse_lazy('Core:ListMateriales')

## DELETE:
class DeleteMateriales(generic.DeleteView):
    model = Materiales
    template_name = "Core/Materiales/Delete.html"
    success_url = reverse_lazy('Core:ListMateriales')

## DETAIL:
class DetailMateriales(generic.DetailView):
    template_name = "Core/Materiales/Detail.html"
    model = Materiales


## RESPUESTAS: -------------------------------------------------------------- ##

## CREATE:
class CreateRespuestas(generic.CreateView):
    model = Respuestas
    form_class = RespuestasForm
    template_name = "Core/Respuestas/Create.html"
    success_url = reverse_lazy('Core:ListRespuestas')

## LIST:
class ListRespuestas(generic.View):
    template_name = "Core/Respuestas/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Respuestas.objects.all()

        self.context = {
            "RPconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateRespuestas(generic.UpdateView):
    model = Respuestas
    form_class = UpdateRespuestasForm
    template_name = "Core/Respuestas/Update.html"
    success_url = reverse_lazy('Core:ListRespuestas')

## DELETE:
class DeleteRespuestas(generic.DeleteView):
    model = Respuestas
    template_name = "Core/Respuestas/Delete.html"
    success_url = reverse_lazy('Core:ListRespuestas')

## DETAIL:
class DetailRespuestas(generic.DetailView):
    template_name = "Core/Respuestas/Detail.html"
    model = Respuestas

## PREGUNTAS: --------------------------------------------------------------- ##

## CREATE:
class CreatePreguntas(generic.CreateView):
    model = Preguntas
    form_class = PreguntasForm
    template_name = "Core/Preguntas/Create.html"
    success_url = reverse_lazy('Core:ListPreguntas')

## LIST:
class ListPreguntas(generic.View):
    template_name = "Core/Preguntas/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Preguntas.objects.all()

        self.context = {
            "PGconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdatePreguntas(generic.UpdateView):
    model = Preguntas
    form_class = UpdatePreguntasForm
    template_name = "Core/Preguntas/Update.html"
    success_url = reverse_lazy('Core:ListPreguntas')

## DELETE:
class DeletePreguntas(generic.DeleteView):
    model = Preguntas
    template_name = "Core/Preguntas/Delete.html"
    success_url = reverse_lazy('Core:ListPreguntas')

## DETAIL:
class DetailPreguntas(generic.DetailView):
    template_name = "Core/Preguntas/Detail.html"
    model = Preguntas

## PREGUNTAS-RESPUESTAS: ---------------------------------------------------- ##

## CREATE:
class CreatePregResp(generic.CreateView):
    model = PregResp
    form_class = PregRespForm
    template_name = "Core/PregResp/Create.html"
    success_url = reverse_lazy('Core:ListPregResp')

## LIST:
class ListPregResp(generic.View):
    template_name = "Core/PregResp/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = PregResp.objects.all()

        self.context = {
            "PEconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdatePregResp(generic.UpdateView):
    model = PregResp
    form_class = UpdatePregRespForm
    template_name = "Core/PregResp/Update.html"
    success_url = reverse_lazy('Core:ListPregResp')

## DELETE:
class DeletePregResp(generic.DeleteView):
    model = PregResp
    template_name = "Core/PregResp/Delete.html"
    success_url = reverse_lazy('Core:ListPregResp')

## DETAIL:
class DetailPregResp(generic.DetailView):
    template_name = "Core/PregResp/Detail.html"
    model = PregResp

## CAPACITACION: ------------------------------------------------------------ ##

## CREATE:
class CreateCapacitacion(generic.CreateView):
    model = Capacitacion
    form_class = CapacitacionForm
    template_name = "Core/Capacitacion/Create.html"
    success_url = reverse_lazy('Core:ListCapacitacion')

## LIST:
class ListCapacitacion(generic.View):
    template_name = "Core/Capacitacion/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Capacitacion.objects.all()

        self.context = {
            "CCconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateCapacitacion(generic.UpdateView):
    model = Capacitacion
    form_class = UpdateCapacitacionForm
    template_name = "Core/Capacitacion/Update.html"
    success_url = reverse_lazy('Core:ListCapacitacion')

## DELETE:
class DeleteCapacitacion(generic.DeleteView):
    model = Capacitacion
    template_name = "Core/Capacitacion/Delete.html"
    success_url = reverse_lazy('Core:ListCapacitacion')

## DETAIL:
class DetailCapacitacion(generic.DetailView):
    template_name = "Core/Capacitacion/Detail.html"
    model = Capacitacion

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['instructores'] = self.object.instructores.all()
        return context

## INSTRUCTOR: -------------------------------------------------------------- ##

## CREATE:
class CreateInstructor(generic.CreateView):
    model = Instructor
    form_class = InstructorForm
    template_name = "Core/Instructor/Create.html"
    success_url = reverse_lazy('Core:ListInstructor')

## LIST:
class ListInstructor(generic.View):
    template_name = "Core/Instructor/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Instructor.objects.all()

        self.context = {
            "ITconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateInstructor(generic.UpdateView):
    model = Instructor
    form_class = UpdateInstructorForm
    template_name = "Core/Instructor/Update.html"
    success_url = reverse_lazy('Core:ListInstructor')

## DELETE:
class DeleteInstructor(generic.DeleteView):
    model = Instructor
    template_name = "Core/Instructor/Delete.html"
    success_url = reverse_lazy('Core:ListInstructor')

## DETAIL:
class DetailInstructor(generic.DetailView):
    template_name = "Core/Instructor/Detail.html"
    model = Instructor

## INSCRIPCION:-------------------------------------------------------------- ##

## CREATE:
class CreateInscripcion(generic.CreateView):
    model = Inscripcion
    form_class = InscripcionForm
    template_name = "Core/Inscripcion/Create.html"
    success_url = reverse_lazy('Core:ListInscripcion')

## LIST:
class ListInscripcion(generic.View):
    template_name = "Core/Inscripcion/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Inscripcion.objects.all()

        self.context = {
            "ICconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateInscripcion(generic.UpdateView):
    model = Inscripcion
    form_class = UpdateInscripcionForm
    template_name = "Core/Inscripcion/Update.html"
    success_url = reverse_lazy('Core:ListInscripcion')

## DELETE:
class DeleteInscripcion(generic.DeleteView):
    model = Inscripcion
    template_name = "Core/Inscripcion/Delete.html"
    success_url = reverse_lazy('Core:ListInscripcion')

## DETAIL:
class DetailInscripcion(generic.DetailView):
    template_name = "Core/Inscripcion/Detail.html"
    model = Inscripcion

## RESPUESTAS-EMPLEADO: ----------------------------------------------------- ##

## CREATE:
class CreateRespEmpleado(generic.CreateView):
    model = RespuestasEmpleado
    form_class = RespEmpleadoForm
    template_name = "Core/RespEmpleado/Create.html"
    success_url = reverse_lazy('Core:ListRespEmpleado')

## LIST:
class ListRespEmpleado(generic.View):
    template_name = "Core/RespEmpleado/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = RespuestasEmpleado.objects.all()

        self.context = {
            "REconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateRespEmpleado(generic.UpdateView):
    model = RespuestasEmpleado
    form_class = UpdateRespEmpleadoForm
    template_name = "Core/RespEmpleado/Update.html"
    success_url = reverse_lazy('Core:ListRespEmpleado')

## DELETE:
class DeleteRespEmpleado(generic.DeleteView):
    model = RespuestasEmpleado
    template_name = "Core/RespEmpleado/Delete.html"
    success_url = reverse_lazy('Core:ListRespEmpleado')

## DETAIL:
class DetailRespEmpleado(generic.DetailView):
    template_name = "Core/RespEmpleado/Detail.html"
    model = RespuestasEmpleado

## -------------------------------------------------------------------------- ##

## CONSTANCIA: -------------------------------------------------------------- ##

## CREATE:
class CreateConstancia(generic.CreateView):
    model = Constancia
    form_class = ConstanciaForm
    template_name = "Core/Constancia/Create.html"
    success_url = reverse_lazy('Core:ListConstancia')

## LIST:
class ListConstancia(generic.View):
    template_name = "Core/Constancia/List.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Constancia.objects.all()

        self.context = {
            "CTconsulta" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateConstancia(generic.UpdateView):
    model = Constancia
    form_class = UpdateConstanciaForm
    template_name = "Core/Constancia/Update.html"
    success_url = reverse_lazy('Core:ListConstancia')

## DELETE:
class DeleteConstancia(generic.DeleteView):
    model = Constancia
    template_name = "Core/Constancia/Delete.html"
    success_url = reverse_lazy('Core:ListConstancia')

## DETAIL:
class DetailConstancia(generic.DetailView):
    template_name = "Core/Constancia/Detail.html"
    model = Constancia

## -------------------------------------------------------------------------- ##