from .models import *
from django.contrib import admin

admin.site.register(Puesto)
admin.site.register(Departamento)
admin.site.register(Empleado)
admin.site.register(Curso)
admin.site.register(Temas)
admin.site.register(Examen)
admin.site.register(Materiales)
admin.site.register(Respuestas)
admin.site.register(Preguntas)
admin.site.register(PregResp)
admin.site.register(Capacitacion)
admin.site.register(Instructor)
admin.site.register(Inscripcion)
admin.site.register(RespuestasEmpleado)
admin.site.register(EmpleadoExamen)
admin.site.register(Constancia)