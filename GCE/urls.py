from django.contrib import admin
from django.urls import include, path

urlpatterns = [
	path('admin/', admin.site.urls),
	path('api/', include("API.urls")),
	path('core/', include("Core.urls")),
	path('', include("Home.urls")), # Campo vacío en lugar de 'home/'.
	]