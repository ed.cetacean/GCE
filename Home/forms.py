from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import UserProfile

class LoginForm(forms.Form):
    username = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type": "text", "class":"form-control", "placeholder":"Escribr el usuario"}))
    password = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type": "password", "class":"form-control", "placeholder":"Escribr la contraseña"}))


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type": "text", "class":"form-control", "placeholder":"Escribr el usuario"}))
    password1 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type": "password", "class":"form-control", "placeholder":"Escribr la contraseña"}))
    password2 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type": "password", "class":"form-control", "placeholder":"Confirme la contraseña"}))
    email = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type": "text", "class":"form-control", "placeholder":"Escribr el email"}))
    first_name = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type": "text", "class":"form-control", "placeholder":"Escribe tu primer nombre"}))
    last_name = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type": "text", "class":"form-control", "placeholder":"Escribe tu segundo nombre"}))
    class Meta:
        model = User
        fields = [
            "username",
            "password1",
            "password2",
            "email",
            "first_name",
            "last_name"
        ]

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = [
            "user",
            "bio",
            "status"
        ]

        widgets = {
            "user": forms.Select(attrs={"type": "select", "class":"form-select"}),
            "bio": forms.Textarea(attrs={"type": "text", "class":"form-control", "placeholder":"Escribe acerca de ti", "row":3}),
            "status": forms.CheckboxInput(attrs={"type": "checkbox", "class":"form-checkbox"})
        }