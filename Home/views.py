from django.forms.models import BaseModelForm
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import generic, View
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .forms import LoginForm, SignUpForm, UserProfileForm
from django.urls import reverse_lazy, reverse
from .models import UserProfile
from django.contrib.auth.mixins import LoginRequiredMixin

class Index(generic.View):
    template_name = "Home/index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "Developer" : "Ed Rubio",
        }
        return render(request, self.template_name, self.context)

class LogIn(generic.View):
    template_name = "Home/login.html"
    form_class = LoginForm
    context = {}


    def get(self, request, *args, **kwargs):
        self.context = {
            "Developer": "Jaime López",
            "form": self.form_class(),
        }
        return render(request, self.template_name, self.context)

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/")

        # Si la autenticación falla o los datos del formulario son inválidos,
        # puedes agregar un mensaje de error o simplemente redirigir al mismo lugar
        return redirect("/")

class LogOut(generic.View):
    def get(self, request):
        logout(request)
        return redirect("/")

class SignUp(generic.CreateView):
    template_name = "Home/signup.html"
    form_class = SignUpForm
    success_url = reverse_lazy("Home:index")

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get("username")
        password1 = form.cleaned_data.get("password1")
        user = authenticate(self.request, username=username, password=password1)
        if user is not None:
            login(self.request, user)
            return redirect("/")

    def get_success_url(self):
        pk = self.kwargs["pk"]
        return reverse("Home:detail_userprofile", kwargs={"pk":pk})

class UserProfile2(generic.CreateView):
    template_name = "Home/user_profile.html"
    model = UserProfile
    form_class = UserProfileForm
    success_url = reverse_lazy("Home:user_profile")
    login_url = "/"

class DetailUserProfile(LoginRequiredMixin, generic.View):
    template_name = "Home/detail_userprofile.html"
    context = {}
    login_url = "/"

    def get(self, request, pk):
        self.context = {
            "profile" : UserProfile.objects.get(id=pk)
        }
        return render(request, self.template_name, self.context)

class UpdateUserProfile(LoginRequiredMixin, generic.UpdateView):
    template_name = "Home/update_userprofile.html"
    model = UserProfile
    form_class = UserProfileForm
    success_url = reverse_lazy("Home:detail_userprofile")
    login_url = "/"

    def get_success_url(self):
        pk = self.kwargs["pk"]
        return reverse("Home:detail_userprofile", kwargs={"pk":pk})

class About(generic.View):
    template_name = "Home/about.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name" : "Jaime López"
        }
        return render(request, self.template_name, self.context)

class capacitacion(generic.View):
    template_name = "Home/capacitaciones.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "Developer" : "HugoGOD",
        }
        return render(request, self.template_name, self.context)

# ---------------------------------------------------------------------------- #