from Home import views
from django.urls import path

app_name = "Home"
urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('signup/', views.SignUp.as_view(), name="signup"),
    path('user_profile/', views.UserProfile2.as_view(), name="user_profile"),
    path('detail/user_profile/<int:pk>/', views.DetailUserProfile.as_view(), name="detail_userprofile"),
    path('update/user_profile/<int:pk>/', views.UpdateUserProfile.as_view(), name="update_userprofile"),
    path('logout/', views.LogOut.as_view(), name="logout"),
    path('login/', views.LogIn.as_view(), name="login"),
    path('about/', views.About.as_view(), name="about"),
    path('capacitacion/', views.capacitacion.as_view(), name="Capacitacion"),
]